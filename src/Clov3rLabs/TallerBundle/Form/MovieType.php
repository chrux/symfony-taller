<?php

namespace Clov3rLabs\TallerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MovieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('releasedAt')
            ->add('duration')
            ->add('rating')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Clov3rLabs\TallerBundle\Entity\Movie'
        ));
    }

    public function getName()
    {
        return 'clov3rlabs_tallerbundle_movietype';
    }
}
