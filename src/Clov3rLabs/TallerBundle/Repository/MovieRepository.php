<?php
/**
 * Created by JetBrains PhpStorm.
 * User: christian
 * Date: 8/2/13
 * Time: 8:48 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Clov3rLabs\TallerBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Clov3rLabs\TallerBundle\Entity\Movie;

class MovieRepository extends EntityRepository
{
    public function count()
    {
        $em = $this->getEntityManager();

        $qb = $em->createQuery("SELECT COUNT(m.id) FROM Clov3rLabsTallerBundle:Movie m");

        $qb->setMaxResults(1);

        try
        {
            $total = $qb->getSingleScalarResult();
        } catch ( \Exception $e )
        {
            $total = 0;
        }

        return $total;
    }
}