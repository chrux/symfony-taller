<?php

namespace Clov3rLabs\TallerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('Clov3rLabsTallerBundle:Default:index.html.twig', array('name' => $name));
    }

    public function redirectResponseAction()
    {
        return new RedirectResponse($this->generateUrl('clov3r_labs_taller_homepage', array('name' => 'River',)));
    }

    public function securityAction()
    {
        // Agarrando el usuario
        $user = $this->get('security.context')->getToken()->getUser();

        // Chequeando si esta autenticadoddd
        if ( $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ) {
            return new Response('Usuario Registrado: ' . $user);
        } else {
            return new Response('Usuario Anónimo: ' . $user);
        }
    }

    public function navBarAction()
    {
        $links = $this
            ->getDoctrine()
            ->getRepository("Clov3rLabsTallerBundle:Link")
            ->findAll();

        return $this->render(
            'Clov3rLabsTallerBundle:Default:navBar.html.twig',
            array(
                'links' => $links,
            )
        );
    }
}
